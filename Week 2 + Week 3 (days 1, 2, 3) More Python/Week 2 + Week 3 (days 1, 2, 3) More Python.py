import sys

#1.Create a class of Bike and Engine and map to mimic a real-world bike object. (Please ponder design options, before implementation).
class Engine:

    def setEngineSpec(self, engineBrand, engineCc):
        self.engineBrand = engineBrand
        self.engineCc = engineCc

    def showEngineSpec(self):
        return self.engineBrand , self.engineCc

class Bike(Engine):

    def __init__(self,bikeBrand, engineBrand, engineCc):
        #Engine.__init__(engineBrand, engineCc)
        super().setEngineSpec(engineBrand, engineCc)
        self.wheel = 2
        self.bikeBrand = bikeBrand

    def showBikeSepc(self):
        print("""Bike Company = {},
        Engine Brand = {}
        EngineCC = {},
        WheelCount = {}
        """.format(self.bikeBrand, super().showEngineSpec()[0] , super().showEngineSpec()[1], self.wheel))

heroBike = Bike('Hero','BMW',100)
heroBike.showBikeSepc()

#2.OOP code to display, lend and receive the books from students in a library.
class Library:
    def __init__(self, listOfBooks):
        self.availableBooks = listOfBooks

    def displayAvailableBooks(self):
        for book in self.availableBooks:
            print(book)

    def lendBooks(self, requestedBook):
        if requestedBook in self.availableBooks:
            print('Successfully Borrowed')
            self.availableBooks.remove(requestedBook)

    def addBooks(self, returnedbook):
        self.availableBooks.append(returnedbook)


class Student:

    def requestBook(self):
        self.requestedBook = input('Enter the name of the book you want to borrow:')
        return self.requestedBook

    def returnBook(self):
        self.returnedBook = input('Enter the name of the book you want to return:')
        return self.returnedBook

def main():
    library = Library(['A','B','C'])
    student = Student()
    done = True

    while True:
        print("""####Welcome####
        1. Display Books
        2. Request a Book
        3. Return a Book
        4. Exit""")
        choice = int(input("Please Enter your Choice: "))
        print("Your Choice is ", choice)
        if choice==1:
            library.displayAvailableBooks()
        elif choice ==2:
            library.lendBooks(student.requestBook())
        elif choice==3:
            library.addBooks(student.returnBook())
        else:
            sys.exit()


main()

#3.Write a program to generate Fibonacci numbers using a generator.
inputNumber = int(input('Enter Number: '))

def Fibonacci(n):
    a, b = 0, 1
    for i in range(n):
        yield a
        a, b = b, a + b

print(list(Fibonacci(inputNumber)))


#4.Write a program that takes a list and iterates it from the reverse direction. (Write iterator class for this).
class iterator:

    def __init__(self):
        self.list = []

    def set_iter_list(self, list):
        self.list = list

    def iter_list(self):
        for i in reversed(self.list):
            print(i)

itr = iterator()
list = [11,14,15,12,9]
itr.set_iter_list(list)
itr.iter_list()




# Databricks notebook source
# MAGIC %md
# MAGIC #1.Create a spark session in data bricks notebook and attach it to a cluster.

# COMMAND ----------

from pyspark.sql import functions as fun
from pyspark.sql.types import StructType,StructField, StringType, IntegerType , DateType, DoubleType
from pyspark.sql.types import ArrayType, DoubleType, BooleanType
from pyspark.sql.functions import col,array_contains

# COMMAND ----------

from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('Walmart_Data_Analysis').getOrCreate()

# COMMAND ----------

# MAGIC %md
# MAGIC #2.Load the given csv file and check its schema. If not correct, then correct the schema and the datatypes of attributes.

# COMMAND ----------

schema = StructType() \
      .add("Date",DateType(),True) \
      .add("Open",DoubleType(),True) \
      .add("High",DoubleType(),True) \
      .add("Low",DoubleType(),True) \
      .add("Close",DoubleType(),True) \
      .add("Volume",IntegerType(),True) \
      .add("Adj Close",DoubleType(),True)

# COMMAND ----------

df = spark.read.option("header",True).option("inferSchema",True).schema(schema).csv(path)
df.show()

# COMMAND ----------

df.printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC #3.Format the ‘Adj Close’ column to show up to two decimal places only.

# COMMAND ----------

df_rounded = df.select("Date","Open","High","Low","Close","Volume",fun.round(fun.col('Adj Close'),2).alias('Adj Close'))

# COMMAND ----------

df_rounded.show()

# COMMAND ----------

df_rounded.printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC #4.Create a new dataframe with a column called ‘high_low_Ratio’ that is the ratio of the High Price versus Low Price of stock.

# COMMAND ----------

df_high_low_ratio = df_rounded.withColumn('HL Ratio', df_rounded['High']/df_rounded['Low']).select(['HL Ratio'])
df_high_low_ratio.show()

# COMMAND ----------

# MAGIC %md
# MAGIC #5.What day had the Peak High in Price?

# COMMAND ----------

df_rounded.orderBy(df_rounded.High.desc()).select(df_rounded.Date, df_rounded.High).show(1)

# COMMAND ----------

# MAGIC %md
# MAGIC #6.Find the mean Volume.

# COMMAND ----------

df_rounded.select(fun.mean(df_rounded.Volume)).show()

# COMMAND ----------

# MAGIC %md
# MAGIC #7.Find the average difference between the High and the Low.

# COMMAND ----------

df_rounded.select(fun.avg(df_rounded.High)-fun.avg(df_rounded.Low)).show()

# COMMAND ----------

# MAGIC %md
# MAGIC #8.Find minimum and maximum values for each numerical column.

# COMMAND ----------

df_rounded.select(fun.min(df_rounded.Open),fun.min(df_rounded.High),fun.min(df_rounded.Low),fun.min(df_rounded.Close),fun.min(df_rounded.Volume),fun.min(df_rounded['Adj Close']),fun.max(df_rounded.Open),fun.max(df_rounded.High),fun.max(df_rounded.Low),fun.max(df_rounded.Close),fun.max(df_rounded.Volume),fun.max(df_rounded['Adj Close'])).show()

# COMMAND ----------

# MAGIC %md 
# MAGIC #9.Find percentage of the time when the ‘Low’ is lower than 70 dollars.

# COMMAND ----------

low_count = df_rounded.filter(df_rounded.Low < 70).count()
low_count

# COMMAND ----------

total_low_count = df_rounded.count()
total_low_count

# COMMAND ----------

percentage_low_count=(low_count/total_low_count)*100
percentage_low_count

# COMMAND ----------

# MAGIC %md
# MAGIC #10.Find all the types of correlation between ‘Volume’ and ‘High’.

# COMMAND ----------

volume_high_corr = df_rounded.stat.corr('Volume', 'High', "pearson")


# COMMAND ----------

volume_high_corr

# COMMAND ----------

# MAGIC %md
# MAGIC #11.Split the Date column into three columns (day, month, year)

# COMMAND ----------

df_date_split = df_rounded.select("*", fun.dayofmonth(df_rounded["Date"]).alias('day'), fun.month(df_rounded["Date"]).alias('month'),fun.year(df_rounded["Date"]).alias('year'))

# COMMAND ----------

df_date_split.show()

# COMMAND ----------

# MAGIC %md
# MAGIC #12.Find the max High per year and min Low per year.

# COMMAND ----------

df_date_split.groupBy("year").agg({'High':'max','Low':'min'}).show()

# COMMAND ----------

# MAGIC %md
# MAGIC #13.Find the average Close and Volume for each Calendar Month and sort by month.

# COMMAND ----------

df_date_split.groupBy("Month").agg({'Close':'avg','Volume':'avg'}).orderBy(df_date_split.month).show()

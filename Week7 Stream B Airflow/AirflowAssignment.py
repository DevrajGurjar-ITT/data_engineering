from datetime import datetime, timedelta
import requests
from airflow import DAG
from airflow.providers.sqlite.hooks.sqlite import SqliteHook
from airflow.providers.sqlite.operators.sqlite import SqliteOperator
import json
from airflow.operators.python_operator import PythonOperator
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.providers.http.sensors.http import HttpSensor
from airflow.operators.bash import BashOperator

# Default settings applied to all tasks
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}


def _hello_world():
    print('Hello World')


def _read_response(ti):
    response = ti.xcom_pull(
        task_ids='fetch_API_data_using_http_operator',
        key='return_value'
    )
    print(response['results'])
    data = response['results'][0]
    print(data['gender'], data['name']['first'], data['location']['country'])
    gender = data['gender']
    first_name = data['name']['first']
    country = data['location']['country']
    print(first_name, gender, country)
    ti.xcom_push(key="name", value=first_name)
    ti.xcom_push(key="gender", value=gender)
    ti.xcom_push(key="country", value=country)


def http_status(response):
    if response == 200:
        return True
    else:
        return False


with DAG('AirflowAssignment_Dag',
         start_date=datetime(2021, 1, 1),
         max_active_runs=2,
         schedule_interval=timedelta(minutes=30),
         default_args=default_args,
         catchup=False
         ) as dag:
    hello_world = PythonOperator(
        task_id="Hello_World_Task_Using_Python_Operator",
        python_callable=_hello_world,
        dag=dag
    )

    create_Table = SqliteOperator(
        task_id="create_table_using_sqlite_operator",
        sqlite_conn_id="CustomSqlite",
        sql=r"""CREATE TABLE IF NOT EXISTS
        Customers (FirstName TEXT, Gender Text, Country TEXT);
        """,
        dag=dag
    )

    task_http_sensor_check = HttpSensor(
        task_id='check_if_API_available_using_httpSensor_operator',
        http_conn_id='http_default',
        endpoint='api/',
        request_params={},
        response_check=lambda response: True if http_status(response.status_code) is True else False,
        poke_interval=5,
        dag=dag,
    )

    fetch_http_data = SimpleHttpOperator(
        task_id='fetch_API_data_using_http_operator',
        method='GET',
        http_conn_id='http_default',
        endpoint='api/',
        headers={"Content-Type": "application/json"},
        response_filter=lambda response: json.loads(response.content),
        log_response=True,
        dag=dag)

    read_response = PythonOperator(
        task_id='process_fetched_data_using_python_operator',
        python_callable=_read_response,
        dag=dag
    )

    insert_into_database = BashOperator(
        task_id='insert_into_database_using_sqlite_operator',
        bash_command='sqlite3 /c/SnapTest/sqlite_default.db "insert into Customers (FirstName, Gender, Country) '
                     'values(\'{{ ti.xcom_pull(task_ids="process_fetched_data_using_python_operator", key="name") '
                     '}}\',\'{{ ti.xcom_pull( '
                     'task_ids="process_fetched_data_using_python_operator", key="gender") }}\',\'{{ ti.xcom_pull('
                     'task_ids="process_fetched_data_using_python_operator", '
                     'key="country") }}\')"',
        dag=dag
    )

hello_world >> create_Table >> task_http_sensor_check >> fetch_http_data >> read_response >> insert_into_database

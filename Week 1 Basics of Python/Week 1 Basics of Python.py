#Write a program to find common values from two list
list1 = [11,13,14,15]
list2 = [13,14,15,17]
list1 = set(list1)
print(list1.intersection(list2))


#Write a program to remove duplicate elements from the given list with the original order.
list = [11,12,11,41,15,12]
uniquelist = []
duplicate = set()
for item in list:
    if item not in duplicate:
        uniquelist.append(item)
        duplicate.add(item)

print(uniquelist)

#Write a program to sort the list, dictionary
list = [11,14,12,17,14]
list.sort()
print(list)

dictr = {1:11,3:13,3:15,2:12,5:10}
sorted(dictr)
print(dictr)
for i in sorted(dictr):
    print(i , dictr[i])


#Write a program to convert decimal number into octal and vice versa.
num = 123
print(bin(num), oct(num), hex(num), int(oct(num), 8))

#Write a program to print all prime numbers between 0, 10 using list comprehension.
print([x for x in range(2, 20)
     if all(x % y != 0 for y in range(2, x))])


#Write a method to take a list as an input and gives the output as a list of tuples. where each tuple will contain (index, value).
def list_of_tuple(list):
    newlist = []
    i = 0
    for ele in list:
        tup=tuple()
        tup = tup + (i,)
        tup = tup + (ele,)
        newlist.append(tup)
        i+=1
    return newlist

print(list_of_tuple([11,1,13,2,15]))


#Program to accept a matrix of order MxM and interchange the diagonals
array= [1,2,3],[4,5,6],[7,8,9]
print(array)

N = 3;

# Function to interchange diagonals
def interchangeDiagonals(array):
    # swap elements of diagonal
    for i in range(N):
        if (i != N / 2):
            temp = array[i][i];
            array[i][i] = array[i][N - i - 1];
            array[i][N - i - 1] = temp;

    for i in range(N):
        for j in range(N):
            print(array[i][j], end=" ");
        print();


# Driver Code
if __name__ == '__main__':
    array = [4, 5, 6], [1, 2, 3], [7, 8, 9];
    interchangeDiagonals(array);


#Program to accept a text file as input and print the text in a new file with the first alphabet of each word capitalized. Also, print the number of white spaces in the new file.
file = 'test.txt'

with open(file, 'r') as f:
    for line in f:
        newline = line.title()
        print(newline)

f = open('test.txt')
char = f.read()
print(char)
count = 0
for i in char:
    if i == " ":
        count+=1
print(count)
